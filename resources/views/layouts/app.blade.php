<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title','Laravel-Project')</title>

    <!--fonts-->
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet"/>
    @stack('fonts')

    <!--styles-->
    <link rel="stylesheet" href="{{asset('css/main.css')}}"/>
    @stack('styles')

</head>
<body>
    <header>
        <h1>Welcome to Todo-Collection</h1>
        <nav>
            <ul class="hypers">
                <a href="{{asset('/todoes/list')}}"><li><h2>List</h2></li></a>
                <a href="{{asset('/todoes/make')}}"><li><h2>Make</h2></li></a>
            </ul>
        </nav>
    </header>
    @section('body')
        <h1>Welcome to the Laravel</h1>
    @show
    <footer>
        <hr/>
        <h1>Made with interseting ;)</h1>
    </footer>
</body>
</html>


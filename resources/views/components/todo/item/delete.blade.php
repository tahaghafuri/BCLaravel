@aware(['deletePath'])
<div class="delete">
    <a href="{{$deletePath}}">
        <h2>
            Delete
        </h2>
    </a>
</div>